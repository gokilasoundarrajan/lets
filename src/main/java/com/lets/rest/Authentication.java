package com.lets.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lets.service.AuthenticationService;

@RestController
@RequestMapping("/lets")
public class Authentication {
	
	@Autowired
	private AuthenticationService authenticationService;

	
	@GetMapping("/login")
	public String login() {
		return authenticationService.login();
	}
}
